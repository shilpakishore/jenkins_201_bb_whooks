package com.bbWhdemo.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class WhooksServlet
 */
@WebServlet("/WhooksServlet")
public class WhooksServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WhooksServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Date date = new Date();
		long length = Long.parseLong(request.getParameter("Length_rect"));
		long breath = Long.parseLong(request.getParameter("breath_rect"));
		long area = length*breath;
		
			      
		  String title = "Jenkins BB WebHooks Demo ";
	      out.println(
	         "<html>\n" +
	            "<head><title>" + title + "</title></head>\n" +
	            "<body bgcolor = \"#f0f0f0\">\n" +
	               "<h1 align = \"center\">" + title + "</h1>\n" +
	               "<ul>\n" +
	                  "<b>Length of Rectangle </b>: "
	                  + length+ "\n <b>Breath of Rectangle</b>" +
	                  breath + "\n <br>" +
	                  "<b>Area of Rectangle : "+area +
	                  "<b>Today's Date  : "+date.toString() +
	               "</ul>\n" +
	            "</body></html>");
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
